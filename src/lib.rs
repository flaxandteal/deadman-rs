use prometheus::{Encoder, TextEncoder};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use std::time::{Duration, Instant};

use futures::future::{loop_fn, Future, Loop};

use hyper::header::{HeaderValue, CONTENT_TYPE};
use hyper::service::service_fn_ok;
use hyper::{Body, Method, Response, Server, StatusCode};

use mime::Mime;

use slog::{debug, error, info, Level, Logger};

use tokio::timer::Delay;

mod alert;
mod prom;

pub struct Config {
    pub am_url: String,
    pub interval: Duration,
    pub log_level: Level,
    pub service_name: String,
}

pub struct Service {
    name: String,
    prom_handle: prom::PromHandle,
    alerter: alert::AMNotifier,
}

pub struct Deadman {
    pinger: Arc<Pinger>,
    logger: Logger,
    service: Arc<Service>,
}

struct Pinger {
    pinged: AtomicBool,
    interval: u64,
}

impl Pinger {
    fn new(interval: Duration) -> Pinger {
        Pinger {
            pinged: AtomicBool::new(false),
            interval: (interval.as_secs() * 1000 + u64::from(interval.subsec_millis())),
        }
    }
}

impl Deadman {
    pub fn new(
        interval: Duration,
        notifier_url: String,
        logger: Logger,
        service_name: String,
    ) -> Deadman {
        Deadman {
            pinger: Arc::new(Pinger::new(interval)),
            logger,
            service: Arc::new(Service {
                alerter: alert::AMNotifier::new(&notifier_url),
                name: service_name,
                prom_handle: prom::PromHandle::new(),
            }),
        }
    }

    pub fn debug<S>(&mut self, line: S)
    where
        S: Into<String>,
    {
        debug!(self.logger, "{}", line.into());
    }

    pub fn serve_status(&mut self) -> impl futures::future::Future<Item = (), Error = ()> {
        let addr = ([0, 0, 0, 0], 8080).into();

        let logger = self.logger.clone();
        let err_logger = self.logger.clone();

        Server::bind(&addr)
            .serve(move || {
                let logger = logger.clone();
                service_fn_ok(move |req| {
                    debug!(logger, "==>");
                    let encoder = TextEncoder::new();
                    match (req.method(), req.uri().path()) {
                        (&Method::GET, "/metrics") => {
                            debug!(logger, "Received metrics request [status]");
                            let metric_families = prometheus::gather();
                            let mut buffer = vec![];
                            encoder.encode(&metric_families, &mut buffer).unwrap();
                            let mime = encoder.format_type().parse::<Mime>().unwrap();
                            Response::builder()
                                .header(CONTENT_TYPE, HeaderValue::from_str(mime.as_ref()).unwrap())
                                .body(Body::from(buffer))
                                .unwrap()
                        }
                        (&Method::GET, "/healthz") => {
                            debug!(logger, "Received liveness check [status]");
                            Response::builder().body(Body::from("ok")).unwrap()
                        }
                        (&Method::GET, "/") => {
                            debug!(logger, "Received / request [status]");
                            Response::builder().body(Body::from("")).unwrap()
                        }
                        _ => Response::builder()
                            .status(StatusCode::NOT_FOUND)
                            .body(Body::from(""))
                            .unwrap(),
                    }
                })
            })
            .map_err(move |err| {
                error!(err_logger, "Status Server: {}", err);
            })
    }

    pub fn serve(&mut self) -> impl futures::future::Future<Item = (), Error = ()> {
        let addr = ([0, 0, 0, 0], 9095).into();

        let logger = self.logger.clone();
        let pinger = self.pinger.clone();
        let err_logger = self.logger.clone();

        Server::bind(&addr)
            .serve(move || {
                let logger = logger.clone();
                let pinger = pinger.clone();
                service_fn_ok(move |req| {
                    debug!(logger, "-->");

                    match (req.method(), req.uri().path()) {
                        (&Method::GET, "/") => {
                            debug!(logger, "Received / request");
                            Response::builder().body(Body::from("")).unwrap()
                        }
                        (&Method::POST, "/alive") => {
                            debug!(logger, "Received ping");
                            pinger.pinged.store(true, Ordering::Relaxed);
                            Response::builder().body(Body::from("ok")).unwrap()
                        }
                        _ => Response::builder()
                            .status(StatusCode::NOT_FOUND)
                            .body(Body::from(""))
                            .unwrap(),
                    }
                })
            })
            .map_err(move |err| {
                error!(err_logger, "Server: {}", err);
            })
    }

    pub fn start(&self) -> impl futures::future::Future<Item = (), Error = ()> {
        let service = self.service.clone();
        let logger = self.logger.clone();
        let pinger = self.pinger.clone();

        debug!(self.logger, "Starting loop...");
        debug!(self.logger, "Interval: {}s", pinger.interval % 1000);

        loop_fn(pinger.interval, move |_| {
            let service = service.clone();
            let logger = logger.clone();
            let pinger = pinger.clone();
            Delay::new(Instant::now() + Duration::from_millis(pinger.interval))
                .and_then(move |_| {
                    service.prom_handle.total.inc();
                    if !pinger.pinged.load(Ordering::Relaxed) {
                        debug!(
                            logger,
                            "Ticks Notified: {}",
                            service.prom_handle.notified.get()
                        );
                        let _notify = service
                            .alerter
                            .notify(alert::Alert::default(&service.name))
                            .map_err(|_| {
                                debug!(logger, "Notification Failed");
                                service.prom_handle.failed.inc();
                            });
                    } else {
                        info!(logger, "Ping received...");
                    }
                    pinger.pinged.store(false, Ordering::Relaxed);
                    Ok(Loop::Continue(pinger.interval))
                })
                .map_err(|err| {
                    panic!("{} Delay Failed?!", err);
                })
        })
    }
}

pub fn run(cfg: Config, logger: slog::Logger) -> Result<(), String> {
    let mut deadman = Deadman::new(cfg.interval, cfg.am_url, logger, cfg.service_name);

    deadman.debug("[starting event loop]");
    hyper::rt::run(hyper::rt::lazy(move || {
        deadman.debug("[spawning counter]");
        hyper::rt::spawn(deadman.start());
        deadman.debug("[spawning serve]");
        hyper::rt::spawn(deadman.serve());
        deadman.debug("[spawning status serve]");
        hyper::rt::spawn(deadman.serve_status());
        deadman.debug("[spawns complete]");
        Ok(())
    }));

    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
