use futures::future::Future;
use hyper::header::{HeaderValue, CONTENT_TYPE};
use hyper::{Body, Method};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::time::Duration;
use tokio::prelude::FutureExt;

struct AlertBuilder {
    service_name: String,
    status: String,
    labels: serde_json::Map<String, serde_json::Value>,
    annotations: serde_json::Map<String, serde_json::Value>,
}

impl AlertBuilder {
    fn new<S>(service_name: S) -> AlertBuilder
    where
        S: Into<String>,
    {
        AlertBuilder {
            service_name: service_name.into(),
            status: "".to_string(),
            labels: serde_json::Map::new(),
            annotations: serde_json::Map::new(),
        }
    }

    fn status<S>(mut self, status: S) -> AlertBuilder
    where
        S: Into<String>,
    {
        self.status = status.into();
        self
    }

    fn label<S>(mut self, label_name: S, label_value: serde_json::Value) -> AlertBuilder
    where
        S: Into<String>,
    {
        self.labels.insert(label_name.into(), label_value);
        self
    }

    fn annotation<S>(
        mut self,
        annotation_name: S,
        annotation_value: serde_json::Value,
    ) -> AlertBuilder
    where
        S: Into<String>,
    {
        self.annotations
            .insert(annotation_name.into(), annotation_value);
        self
    }

    fn build(self) -> Alert {
        Alert::new(
            self.service_name,
            self.status,
            self.labels,
            self.annotations,
        )
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Alert {
    service_name: String,
    status: String,
    labels: serde_json::Map<String, serde_json::Value>,
    annotations: serde_json::Map<String, serde_json::Value>,
}

impl Alert {
    fn new<S>(
        service_name: S,
        status: S,
        labels: serde_json::Map<String, serde_json::Value>,
        annotations: serde_json::Map<String, serde_json::Value>,
    ) -> Alert
    where
        S: Into<String>,
    {
        Alert {
            service_name: service_name.into(),
            status: status.into(),
            labels,
            annotations,
        }
    }

    pub fn default(service_name: &str) -> Alert {
        AlertBuilder::new(service_name)
            .status("firing")
            .label("alertname", json!("DeadmanDead"))
            .label("summary", json!(format!("Deadman: {}", service_name)))
            .annotation(
                "description",
                json!(format!(
                    "Deadman's switch triggered for service {}",
                    service_name
                )),
            )
            .build()
    }
}

pub struct AMNotifier {
    uri: hyper::Uri,
}

impl AMNotifier {
    pub fn new(uri: &str) -> AMNotifier {
        AMNotifier {
            uri: uri.parse().unwrap(),
        }
    }

    pub fn notify(&self, alert: Alert) -> Result<(), ()> {
        let connector = hyper::client::HttpConnector::new(1);
        let client = hyper::client::Client::builder().build(connector);
        let uri = &self.uri;

        let alerts = &vec![alert];

        let body = match serde_json::to_string(alerts) {
            Ok(value) => value,
            Err(_) => return Err(()),
        };

        let req = hyper::Request::builder()
            .method(Method::POST)
            .uri(uri)
            .header(CONTENT_TYPE, HeaderValue::from_static("application/json"))
            .body(Body::from(body))
            .unwrap();

        // FIXME: TIMEOUT as setting
        let fut = client
            .request(req)
            .timeout(Duration::from_secs(3))
            .then(|result| match result {
                Ok(_) => Ok(()),
                Err(_) => {
                    println!("ALERTMANAGER ERROR");
                    Err(())
                }
            });
        hyper::rt::spawn(fut);
        Ok(())
    }
}
