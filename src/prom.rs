use prometheus::Counter;
use prometheus::{labels, opts, register_counter};

pub struct PromHandle {
    pub total: Counter,
    pub notified: Counter,
    pub failed: Counter,
}

impl PromHandle {
    pub fn new() -> PromHandle {
        PromHandle {
            total: register_counter!(opts!(
                "deadman_ticks_total",
                "The total ticks passed in this snitch",
                labels! {"handler" => "all",}
            ))
            .unwrap(),
            notified: register_counter!(opts!(
                "deadman_ticks_notified",
                "The number of ticks where notifications were sent.",
                labels! {"handler" => "all",}
            ))
            .unwrap(),
            failed: register_counter!(opts!(
                "deadman_notifications_failed",
                "The number of failed notifications.",
                labels! {"handler" => "all",}
            ))
            .unwrap(),
        }
    }
}
