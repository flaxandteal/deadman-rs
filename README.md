# Deadman Switch in Rust

This utility expects regular messages from a Prometheus Alertmanager alerting pipeline, generally on some other infrastructure.
If they do not arrive in a given period, it hits its own Alertmanager instance, usually on the same infrastructure, directly to
trigger any notifications configured there.

![Diagram showing links between components in two clusters - project cluster and core cluster: project-prometheus...project-alertmanager...core-deadman...core-alertmanager...rocketchat](docs/linkage.png)

### Acknowledgements

This tool was inspired by the Go-based [deadman switch](https://github.com/gouthamve/deadman) made by [gouthamve](https://github.com/gouthamve/)
