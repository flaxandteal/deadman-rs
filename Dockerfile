FROM scratch

ADD ./target/x86_64-unknown-linux-musl/release/deadman /

USER 1000

EXPOSE 9095

ENTRYPOINT ["/deadman"]
